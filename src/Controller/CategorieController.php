<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categorie;
 

class CategorieController extends AbstractController
{
    
    /**
     * @Route("/categorie/", name="addCategorie", methods={"POST"})
     */
    public function addCategorie(Request $request): Response
    {
        $data = json_decode($request->getContent(),true);

        $em = $this->getDoctrine()->getManager();

        $categorie = new Categorie();
        if(isset($data["name"]) && is_string($data["name"])){
            $name = $data["name"];
        }else{
            return $this->json([
                'message' => 'Pas de nom, création impossible',
            ]);
        }
        $categorie->setNom($name);

        if(isset($data["motherName"])) {
            if(is_string($data["motherName"])){
                $categorieMere = new Categorie();
                $motherName = $data["motherName"];
                $categorieMere = $em->getRepository("App:Categorie")->findOneByName($motherName);
                if(!isset($categorieMere)){
                    return $this->json([
                        'message' => "La categorie mere n'existe pas",
                    ]);
                }
                $categorie->setMere($categorieMere);
            }else{
                return $this->json([
                    'message' => "l'attribut de nom de categorie mere",
                ]);
            }
        }

        $em->persist($categorie);
        $em->flush();
        return $this->json([
            'message' => 'Welcome to your new controller!',
        ]);
    }

    /**
     * @Route("/categorie/{nom}", name="delCategorie", methods="DELETE")
     */
    public function deleteCategorie(string $nom): Response
    {
        $categorie = new Categorie();
        $em = $this->getDoctrine()->getManager();

        $categorie = $em->getRepository("App:Categorie")->findOneByName($nom);
        if(isset($categorie)){
            $this->deleteFillesRecursively($categorie);
            $this->deleteVehicules($categorie);
            $em->remove($categorie);
            $em->flush();
            return $this->json([
                'message' => 'Entite Supprimee',
            ]);
        }
        return $this->json([
            'message' => 'Impossible de supprimer l"entite!',
        ]);
    }

    public function deleteFillesRecursively(Categorie $categorie){
        $em = $this->getDoctrine()->getManager();

        $categories = $categorie->getFilles();
        foreach($categories as $laCategorie){
                $this->deleteFillesRecursively($laCategorie);
                $em->remove($laCategorie);
        }
    }

    public function deleteVehicules(Categorie $categorie){
        $em = $this->getDoctrine()->getManager();

        $vehicules = $categorie->getVehicules();
        foreach($vehicules as $vehicule){
            $em->remove($vehicule);
        }
    }


/**
     * @Route("/categorie/{name}", name="categorieVehicules", methods={"GET"})
     */
    public function categorieVehicule(Request $request, string $name): Response
    {
        $data = json_decode($request->getContent(),true);

        $em = $this->getDoctrine()->getManager();

        $categorie = new Categorie();
        if(!isset($name) || !is_string($name)){
            return $this->json([
                'message' => 'Pas de nom, création impossible',
            ]);
        }
        $categorie = $em->getRepository("App:Categorie")->findOneByName($name);
        $nbVehicules = 0;
        $this->countVehiculesRecursively($categorie, $nbVehicules);

        return $this->json([
            'message' => $nbVehicules,
        ]);
    }

    public function countVehiculesRecursively(Categorie $categorie, &$nbVehicules){
        $em = $this->getDoctrine()->getManager();
        $nbVehicules += count($categorie->getVehicules());
        $categories = $categorie->getFilles();
        foreach($categories as $laCategorie){
                $this->countVehiculesRecursively($laCategorie,$nbVehicules);
        }
    }




}
