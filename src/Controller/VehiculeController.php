<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Vehicule;
use App\Entity\Categorie;



class VehiculeController extends AbstractController
{
    /**
     * @Route("/vehicule", name="addVehicule", methods={"POST"})
     */
    public function addVehicule(Request $request): Response
    {
        $data = json_decode($request->getContent(),true);

        $em = $this->getDoctrine()->getManager();

        $vehicule = new Vehicule();
        if(isset($data["name"]) && is_string($data["name"])){
            $name = $data["name"];
        }else{
            return $this->json([
                'message' => 'Pas de nom, création impossible',
            ]);
        }
        $vehicule->setName($name);

        if(isset($data["categorie"]) && is_string($data["categorie"])){
            $categorie = new Categorie();
            $nom = $data["categorie"];
            $categorie = $em->getRepository("App:Categorie")->findOneByName($nom);
            $vehicule->setCategorie($categorie);
        }else{
            return $this->json([
                'message' => "La categorie n'existe pas",
            ]);
        }

        $em->persist($vehicule);
        $em->flush();
        return $this->json([
            'message' => 'Vehicule ajoute',
            'path' => 'src/Controller/CategorieController.php',
        ]);
    }

    /**
     * @Route("/vehicule", name="delVehicule", methods={"DELETE"})
     */
    public function delVehicule(Request $request): Response
    {
        $data = json_decode($request->getContent(),true);

        $em = $this->getDoctrine()->getManager();

        $vehicule = new Vehicule();
        if(isset($data["name"]) && is_string($data["name"])){
            $name = $data["name"];
        }else{
            return $this->json([
                'message' => 'Pas de nom, suppression impossible',
            ]);
        }
        $vehicule = $em->getRepository("App:Vehicule")->findOneByName($name);
        $em->remove($vehicule);
        $em->flush();

        return $this->json([
            'message' => 'Vehicule supprimé',
        ]);
    }
}
