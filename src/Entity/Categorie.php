<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="Filles")
     */
    private $Mere;

    /**
     * @ORM\OneToMany(targetEntity=Categorie::class, mappedBy="Mere")
     */
    private $Filles;

    /**
     * @ORM\OneToMany(targetEntity=Vehicule::class, mappedBy="Categorie")
     */
    private $vehicules;

    public function __construct()
    {
        $this->Filles = new ArrayCollection();
        $this->vehicules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getMere(): ?self
    {
        return $this->Mere;
    }

    public function setMere(?self $Mere): self
    {
        $this->Mere = $Mere;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFilles(): Collection
    {
        return $this->Filles;
    }

    public function addFille(self $fille): self
    {
        if (!$this->Filles->contains($fille)) {
            $this->Filles[] = $fille;
            $fille->setMere($this);
        }

        return $this;
    }

    public function removeFille(self $fille): self
    {
        if ($this->Filles->removeElement($fille)) {
            // set the owning side to null (unless already changed)
            if ($fille->getMere() === $this) {
                $fille->setMere(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Vehicule[]
     */
    public function getVehicules(): Collection
    {
        return $this->vehicules;
    }

    public function addVehicule(Vehicule $vehicule): self
    {
        if (!$this->vehicules->contains($vehicule)) {
            $this->vehicules[] = $vehicule;
            $vehicule->setCategorie($this);
        }

        return $this;
    }

    public function removeVehicule(Vehicule $vehicule): self
    {
        if ($this->vehicules->removeElement($vehicule)) {
            // set the owning side to null (unless already changed)
            if ($vehicule->getCategorie() === $this) {
                $vehicule->setCategorie(null);
            }
        }

        return $this;
    }
}
