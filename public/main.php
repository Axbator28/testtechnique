<?php

function calculateGain($arrayDays, $ability){
    // En premier creer un deuxième tableau triers grand au plus petit
    // On parcours ce tableau dans l'ordre. pour chaque élément, on regarde si il est possible de le placer dans les concours participé
    // Pour cela, on regarde si il est join à un autre concours passé. Si oui, on regarde que l'intervalle agrandi ne dépasse pas la limite
    // Sinon, le concours sera ignoré
    $sortedArray = $arrayDays;
    arsort($sortedArray);
    $participation = array_fill(1,count($arrayDays)-1,0);
    $participation[0] = 1;
    //print_r($participation);
    //print_r($participation[0]);
    //print_r($sortedArray);
    //print_r(array_values($sortedArray)[0]);
    $last = -1; //la valeur du dernier concours vérifié, pour ne pas compter de doublons
    $doublon = 0;
    for($i=0; $i<count($sortedArray);$i++){
        $current = array_values($sortedArray)[$i];
        if($i>0 && $current == $last){
            $doublon++;
        }else{
            $doublon=0;
        }
        $currentIndex = array_keys($arrayDays, $current)[$doublon];
        participateIfPossible($participation,$currentIndex, $ability);
        
        $last = $current;
    }
    //print_r($participation);
    $gain = 0;
    for($i=0; $i<count($participation);$i++){
        if($participation[$i]){
            $gain += $arrayDays[$i];
        }
    }
    var_dump($gain);
}

// Fonction qui a pour but de faire participer au concours au rang Index si c'est possible
function participateIfPossible(&$participation, $index, $ability){
    if (!isNeighbour($participation,$index)){
        $participation[$index] = 1;
    }else{
        if(intervalePossible($participation,$index,$ability)){
            $participation[$index] = 1;
        }
    }
}

// Fonction qui permet de montrer si un jour est voisin d'un intervale. Si non, on pourra participer au concours sans hésiter
// Car on les parcourt dans l'ordre
function isNeighbour($participation, $index){
    if( ($index==0 || $participation[$index-1]==0)&&( $index == (count($participation)-1) || $participation[$index+1]==0 )){
        return false;
    }
    return true;
}

// Fonction qui vérifie que, si on participe au concours, l'intervale créé respecte la règle de capacité de la personne
function intervalePossible($participation, $index, $ability){
    $tailleIntervalle = 1;
    $parcourDroite = true;
    $parcourGauche = true;
    $i=1;
    //var_dump($index);
    //var_dump($participation);

    while($parcourGauche && $i<$ability+1 && $index-$i >=0){
        if($participation[$index-$i]){
            $tailleIntervalle++;
        }else{
           $parcourGauche = false; 
        }
        $i++;
    }
    //var_dump($tailleIntervalle);
    $i=1;
    while($parcourDroite && $i<$ability+1 && $index+$i <count($participation)){
        if($participation[$index+$i]){
            $tailleIntervalle++;
        }else{
           $parcourDroite = false; 
        }
        $i++;
    }
    //var_dump($tailleIntervalle);
    //var_dump($tailleIntervalle >$ability);
    //var_dump("_______________________");

    if($tailleIntervalle >$ability){
        return false;
    }else{
        return true;
    }
}


$array = [13, 2, 15, 17, 19, 33, 2,2,2,2];
calculateGain($array,4);
$array = [13,12,11,9,16,17,100];
calculateGain($array,3);
$array = [12,14,52,7,3,1,1,89,98,100,12,5,6,8];
calculateGain($array,4);
$array = [10,12,11,9,17,8,13];
calculateGain($array,2);
$array = [20,30,27,24,23,28,33,38,34,35];
calculateGain($array,10);

function procheDeZero($array){
    $min = $array[0];
    $rank = 0;
    for( $i = 1; $i<count($array);$i++){
        if(abs($array[$i])<abs($min) || (abs($array[$i])==abs($min) && $array[$i]> $min )){
            $min = $array[$i];
            $rank = $i;
        }
    }
    return $array[$rank];
}

var_dump(procheDeZero([3,5,2,9]));
var_dump(procheDeZero([3,5,9,-2]));
var_dump(procheDeZero([3,2,5,9,-2]));
var_dump(procheDeZero([3,5,0,-2]));
var_dump(procheDeZero([3,5,0,-2]));


?>